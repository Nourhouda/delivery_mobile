import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/data/repository/orders_repository.dart';
import 'package:flutter/material.dart';

class OrdersListController extends ChangeNotifier {
  List<OrderModel> orders = [];
  bool listLoading = true;

  getListOrders() async {
    listLoading = true;
    orders = await OrdersRepository().getListOrders();
    listLoading = false;
    notifyListeners();
  }
}
