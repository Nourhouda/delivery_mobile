import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/views/orders/orders_list_controller.dart';
import 'package:delivery_app_livreur/widgets/loading_widget.dart';
import 'package:delivery_app_livreur/widgets/order_list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:searchable_listview/searchable_listview.dart';

import '../../constants/colors.dart';

class OrderListScreen extends StatefulWidget {
  const OrderListScreen({Key? key}) : super(key: key);

  @override
  State<OrderListScreen> createState() => _OrderListScreenState();
}

class _OrderListScreenState extends State<OrderListScreen> {
  late OrdersListController ordersListController;

  @override
  void initState() {
    super.initState();
    ordersListController = Provider.of<OrdersListController>(
      context,
      listen: false,
    );
    ordersListController.getListOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrdersListController>(
      builder: (context, provider, child) {
        return SafeArea(
          child: Container(
            color: Colors.white,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 30,
                horizontal: 20,
              ),
              child: provider.listLoading
                  ? const LoadingWidget()
                  : SearchableList<OrderModel>(
                      inputDecoration: InputDecoration(
                        label: const Text('Recherche'),
                        fillColor: const Color(0xffF3F3F3),
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: secondaryColor,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: secondaryColor,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Color(0xffF3F3F3),
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      filter: (q) {
                        return [];
                      },
                      initialList: provider.orders,
                      builder: (item) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(
                                context,
                                '/order-detail',
                                arguments: item,
                              );
                            },
                            child: OrderListItem(
                              orderModel: item,
              
                            ),
                          ),
                        );
                      },
                    ),
            ),
          ),
        );
      },
    );
  }
}
