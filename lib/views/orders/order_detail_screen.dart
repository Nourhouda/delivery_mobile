import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/utils/extensions.dart';
import 'package:delivery_app_livreur/views/orders/order_details_controller.dart';
import 'package:delivery_app_livreur/widgets/loading_popup.dart';
import 'package:delivery_app_livreur/widgets/status_chooser_widget.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class OrderDetailScreen extends StatefulWidget {
  final OrderModel orderModel;
  const OrderDetailScreen({
    Key? key,
    required this.orderModel,
  }) : super(key: key);

  @override
  State<OrderDetailScreen> createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          color: Theme.of(context).primaryColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: const Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Commande: ORD-${widget.orderModel.shortId()}',
                          style:
                              Theme.of(context).textTheme.subtitle2?.copyWith(
                                    color: Colors.white,
                                  ),
                        ),
                        Text(
                          'Statut de Commande ${widget.orderModel.getStatusLabel()}',
                          style:
                              Theme.of(context).textTheme.subtitle1?.copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        preferredSize: const Size.fromHeight(100),
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Détail du commande',
                              style: Theme.of(context).textTheme.caption,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            ...widget.orderModel.produits.map((produit) {
                              return Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          'x${produit.quantity} ${produit.produit.nomProd}'),
                                      Text('${produit.produit.prix} DT'),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              );
                            }).toList()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text('Sous totale'),
                            Text('${widget.orderModel.totalAmount()} DT'),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Text('Frais de livraison'),
                            Text('7 DT'),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Totale',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            Text(
                              '${widget.orderModel.totalAmount() + 7} DT',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Détail du boutique',
                              style: Theme.of(context).textTheme.caption,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Nom'),
                                Text('${widget.orderModel.store?.nomBoutique}'),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Adresse'),
                                Text(
                                    '${widget.orderModel.store?.adresseSiege}'),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Nom gérant'),
                                Text(
                                    '${widget.orderModel.store?.nomResponsable}'),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Numéro téléphone'),
                                OutlinedButton(
                                  child: Row(
                                    children: [
                                      const Icon(Icons.phone),
                                      Text(
                                          '${widget.orderModel.store?.telResponsable}'),
                                    ],
                                  ),
                                  onPressed: () async {
                                    await launchUrl(
                                      Uri.parse('tel:+216 45 477 788'),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Détail du livraison',
                              style: Theme.of(context).textTheme.caption,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Client'),
                                Text('${widget.orderModel.client?.fullName}'),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Numéro téléphone'),
                                OutlinedButton(
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.phone,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        '${widget.orderModel.client?.numTel}',
                                        style: const TextStyle(
                                          color: Colors.green,
                                        ),
                                      ),
                                    ],
                                  ),
                                  onPressed: () async {
                                    await launchUrl(
                                        Uri.parse('tel:+216 45 477 788'));
                                  },
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text('Adresse livraison'),
                                Text('${widget.orderModel.client?.adresse}'),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 10,
                  ),
                  child: ElevatedButton(
                    onPressed: () {
                      StatusChooserWidget(
                        onConfirmPressed: (status) {
                          widget.orderModel.status = status;
                          const LoadingPopup().show(context);
                          OrderDetailsController()
                              .updateOrderStatus(widget.orderModel, (value) {
                            Navigator.pop(context);
                            if (value == null) {
                              context.displaySnackbar(
                                'Erreur est survenue, veuillez ressayer plus tard',
                                Colors.red,
                              );
                            } else {
                              Navigator.pop(context);
                              context.displaySnackbar(
                                'Commande mis à jour',
                                Colors.green,
                              );
                            }
                          });
                        },
                      ).show(context);
                    },
                    child: const Text('Changer status'),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
