import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/data/repository/orders_repository.dart';

class OrderDetailsController {
  OrdersRepository ordersRepository = OrdersRepository();

  void updateOrderStatus(
      OrderModel order, Function(dynamic) resultHandlerCallback) async {
    var result = await ordersRepository.updateOrderStatus(order);
    resultHandlerCallback.call(result);
  }
}
