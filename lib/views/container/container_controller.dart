import 'package:delivery_app_livreur/data/repository/login_repository.dart';

class ContainerController {
  
  Future<dynamic> updateUserStatus()async {
    return LoginRepository().updateUserStatus();
  }

}