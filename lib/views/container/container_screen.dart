import 'package:delivery_app_livreur/utils/extensions.dart';
import 'package:delivery_app_livreur/views/container/container_controller.dart';
import 'package:delivery_app_livreur/views/orders/orders_list_screen.dart';
import 'package:delivery_app_livreur/views/profile/contact_screen.dart';
import 'package:delivery_app_livreur/views/profile/profile_info_screen.dart';
import 'package:delivery_app_livreur/views/profile/menu_screen.dart';
import 'package:delivery_app_livreur/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

class ContainerScreen extends StatefulWidget {
  const ContainerScreen({Key? key}) : super(key: key);

  @override
  State<ContainerScreen> createState() => _ContainerScreenState();
}

class _ContainerScreenState extends State<ContainerScreen> {
  int index = 0;
  bool livreurStatus = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: const Text('Delivery app livreur'),
        actions: [
          Row(
            children: [
              Text(livreurStatus ? 'Disponible' : 'Indisponible'),
              Switch(
                value: livreurStatus,
                onChanged: (status) async {
                  const LoadingPopup().show(context);
                  var serverResponse =
                      await ContainerController().updateUserStatus();
                  Navigator.pop(context);
                  if (serverResponse != null) {
                    setState(() {
                      livreurStatus = status;
                    });
                    context.displaySnackbar(
                        'Votre statut a été mis à jour', Colors.green);
                  }
                },
              )
            ],
          )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: (i) {
          setState(() {
            index = i;
          });
        },
        currentIndex: index,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white.withOpacity(0.3),
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.list),
            label: 'Mes commandes',
            backgroundColor: Theme.of(context).primaryColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person),
            label: 'Mon profile',
            backgroundColor: Theme.of(context).primaryColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.message),
            label: 'Réclamation',
            backgroundColor: Theme.of(context).primaryColor,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.menu),
            label: 'Menu',
            backgroundColor: Theme.of(context).primaryColor,
          ),
        ],
      ),
      body: _renderScreen(),
    );
  }

  Widget _renderScreen() {
    if (index == 3) {
      return const MenuScreen();
    } else if (index == 1) {
      return const ProfileInfoScreen();
    } else if (index == 2) {
      return const ContactScreen();
    } else {
      return const OrderListScreen();
    }
  }
}
