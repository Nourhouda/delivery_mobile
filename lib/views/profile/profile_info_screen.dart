import 'package:delivery_app_livreur/views/profile/profile_info_controller.dart';
import 'package:delivery_app_livreur/widgets/app_button.dart';
import 'package:delivery_app_livreur/widgets/app_text_fields.dart';
import 'package:delivery_app_livreur/widgets/loading_popup.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileInfoScreen extends StatefulWidget {
  const ProfileInfoScreen({Key? key}) : super(key: key);

  @override
  State<ProfileInfoScreen> createState() => _ProfileInfoScreenState();
}

class _ProfileInfoScreenState extends State<ProfileInfoScreen> {
  late ProfileInfoController infoController;

  @override
  void initState() {
    super.initState();
    infoController = Provider.of<ProfileInfoController>(
      context,
      listen: false,
    );
    infoController.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ProfileInfoController>(
      builder: (context, provider, child) {
        return Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Image.asset(
                'assets/images/profile_info.png',
                width: 250,
              ),
              Text(
                'Mon compte',
                style: Theme.of(context).textTheme.headline6,
              ),
              Expanded(
                child: ListView(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AppTextField(
                          placeholder: 'Email',
                          controller: provider.emailController,
                          enabled: false,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Nom',
                          controller: provider.nameController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Prénom',
                          controller: provider.lastnameController,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AppTextField(
                          placeholder: 'Adresse',
                          controller: provider.adressController,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 10,
                          ),
                          child: AppButton(
                            buttonText: 'Mettre à jour',
                            onTap: () async {
                              const LoadingPopup().show(context);
                              await provider.updateUser();
                              Navigator.pop(context);
                            },
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
