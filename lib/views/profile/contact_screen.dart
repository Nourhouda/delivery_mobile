import 'package:delivery_app_livreur/utils/extensions.dart';
import 'package:delivery_app_livreur/views/profile/contact_screen_controller.dart';
import 'package:delivery_app_livreur/widgets/app_button.dart';
import 'package:delivery_app_livreur/widgets/app_text_fields.dart';
import 'package:delivery_app_livreur/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key? key}) : super(key: key);

  @override
  State<ContactScreen> createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  late ContactScreenController screenController;

  @override
  void initState() {
    super.initState();
    screenController = ContactScreenController();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Image.asset(
              'assets/images/contact_image.png',
              width: 200,
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              'Contactez nous',
              style: Theme.of(context).textTheme.headline6,
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: Text(
                'Veuillez saisir ci-dessous le titre et description et nous envoyer vos réclamation',
                style: Theme.of(context).textTheme.subtitle2,
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            Expanded(
              child: ListView(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      AppTextField(
                        placeholder: 'Titre',
                        controller: screenController.titleController,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      AppTextField(
                        placeholder: 'Description',
                        controller: screenController.descriptionController,
                        height: 100,
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      AppButton(
                        buttonText: 'Envoyer',
                        onTap: _onSubmitPressed,
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onSubmitPressed() async {
    if (screenController.isValidInput()) {
      const LoadingPopup().show(context);
      await screenController.addNewReclamation();
      context.displaySnackbar(
        'Réclamation envoyé',
        Colors.green,
      );
      Navigator.pop(context);
      Navigator.pop(context);
    } else {
      context.displayErrorSnackbar('Veuillez saisir tout les champs');
    }
  }
}
