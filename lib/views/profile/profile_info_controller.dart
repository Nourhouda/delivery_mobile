import 'dart:convert';

import 'package:delivery_app_livreur/constants/shared_prefs_keys.dart';
import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/login_repository.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileInfoController extends ChangeNotifier {
  late UserModel userModel;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController lastnameController = TextEditingController();
  final TextEditingController adressController = TextEditingController();

  initState() async {
    var sharedPrefs = await SharedPreferences.getInstance();
    var userInShared = sharedPrefs.getString(userKey);
    userModel = UserModel.fromJson(jsonDecode(userInShared ?? ''));
    emailController.text = userModel.email;
    nameController.text = userModel.firstname;
    lastnameController.text = userModel.lastName;
    adressController.text = userModel.adresse;
  }

  Future<UserModel?> updateUser() async {
    return LoginRepository().updateUser(
      nameController.text,
      lastnameController.text,
      adressController.text,
    );
  }
}
