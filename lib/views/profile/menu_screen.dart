import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuScreen extends StatelessWidget {
  const MenuScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Column(
          children: [
            Stack(
              children: [
                CachedNetworkImage(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                  imageUrl:
                      'https://images.pexels.com/photos/5025669/pexels-photo-5025669.jpeg',
                  placeholder: (context, url) => Container(),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.black.withOpacity(0.4),
                  child: Center(
                    child: Text(
                      'Mon compte',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          ?.copyWith(color: Colors.white),
                    ),
                  ),
                )
              ],
            ),
            Expanded(
              child: ListView(
                children: [
                  ListTile(
                    leading: const Icon(CupertinoIcons.info),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('A propos'),
                    onTap: () {
                      _displayAboutPopup(context);
                    },
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(CupertinoIcons.star),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Rate our app'),
                    onTap: () {},
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  ListTile(
                    leading: const Icon(Icons.logout),
                    trailing: const Icon(Icons.keyboard_arrow_right),
                    title: const Text('Se déconnecter'),
                    onTap: () {
                      _displayLogoutPopup(context);
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _displayAboutPopup(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("A propos de l'application"),
          content: Container(
            color: Colors.white,
            height: 100,
            child: Column(
              children: const [
                Text('Delivery App'),
                Text('Version 1.0.0'),
                Text(
                  'Designed and developped by Magma Center',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _displayLogoutPopup(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text("Déconnexion"),
          content: Text(
            'Voulez-vous vraiment déconnecter du compte ?',
            style: Theme.of(context).textTheme.caption,
          ),
          actions: [
            TextButton(
              child: Text(
                'Annuler',
                style: TextStyle(color: Colors.blue.withOpacity(0.6)),
              ),
              onPressed: () {},
            ),
            TextButton(
              child: const Text(
                'Déconnecter',
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () async {
                var sharedPrefs = await SharedPreferences.getInstance();
                await sharedPrefs.clear();
                Navigator.pushReplacementNamed(context, '/login');
              }
            ),
          ],
        );
      },
    );
  }
}
