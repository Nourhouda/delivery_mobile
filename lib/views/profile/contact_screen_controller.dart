import 'package:delivery_app_livreur/data/models/reclamation_model.dart';
import 'package:delivery_app_livreur/data/repository/reclamation_repository.dart';
import 'package:flutter/cupertino.dart';

class ContactScreenController {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();

  bool isValidInput() {
    return titleController.text.isNotEmpty &&
        descriptionController.text.isNotEmpty;
  }

  Future<ReclamationModel?> addNewReclamation() async {
    return ReclamationRepository().createNewReclamation(
      titleController.text,
      descriptionController.text,
    );
  }
}
