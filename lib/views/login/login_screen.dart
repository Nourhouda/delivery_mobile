// import 'package:delivery_app_livreur/utils/extensions.dart';
// import 'package:delivery_app_livreur/views/login/login_controller.dart';
// import 'package:delivery_app_livreur/widgets/loading_popup.dart';
// import 'package:flutter/material.dart';

// import '../../widgets/app_button.dart';
// import '../../widgets/app_text_fields.dart';

// class LoginScreen extends StatefulWidget {
//   const LoginScreen({Key? key}) : super(key: key);

//   @override
//   State<LoginScreen> createState() => _LoginScreenState();
// }

// class _LoginScreenState extends State<LoginScreen> {
//   LoginController loginController = LoginController();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Container(
//           color: Colors.white,
//           width: MediaQuery.of(context).size.width,
//           child: Padding(
//             padding: const EdgeInsets.all(10),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Image.asset('assets/logo/delivery_app_livreur_logo.png'),
//                     RichText(
//                       text: const TextSpan(
//                         children: [
//                           TextSpan(
//                             text: 'Welcome to ',
//                             style: TextStyle(
//                               color: Colors.black,
//                               fontSize: 18,
//                             ),
//                           ),
//                           TextSpan(
//                             text: 'delivery platform',
//                             style: TextStyle(
//                               color: Color(0xff61C002),
//                               fontSize: 18,
//                             ),
//                           ),
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//                 Expanded(
//                   child: Align(
//                     alignment: Alignment.center,
//                     child: Column(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         const Text(
//                           'Login now',
//                           style: TextStyle(
//                               color: Colors.black,
//                               fontWeight: FontWeight.bold,
//                               fontSize: 18),
//                         ),
//                         const Text(
//                           'Please enter your credentials',
//                           style: TextStyle(
//                             color: Colors.black,
//                           ),
//                         ),
//                         const SizedBox(
//                           height: 20,
//                         ),
//                         AppTextField(
//                           placeholder: 'Email',
//                           controller: loginController.emailTextController,
//                         ),
//                         const SizedBox(
//                           height: 10,
//                         ),
//                         AppTextField(
//                           placeholder: 'Password',
//                           controller: loginController.passwordTextController,
//                           // obscureText: true,
//                         ),
//                         const SizedBox(
//                           height: 40,
//                         ),
//                         AppButton(
//                           buttonText: 'Sign in',
//                           onTap: _onSubmitPressed,
//                         ),
//                       ],
//                     ),
//                   ),
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   void _onSubmitPressed() {
//     bool validationResult = loginController.verifyInput();
//     if (!validationResult) {
//       const LoadingPopup().show(context);
//       loginController.loginUser().then((result) {
//         Navigator.pop(context);
//         FocusScope.of(context).requestFocus(FocusNode());
//         if (result != null) {
//           Navigator.pushReplacementNamed(context, '/container');
//         } else {
//           context.displayErrorSnackbar('Veuillez vérifier vos coordonnées');
//         }
//       });
//     } else {
//       context.displayErrorSnackbar('Veuillez entrer tout vos coordonnées');
//     }
//   }
// }
import 'package:delivery_app_livreur/utils/extensions.dart';
import 'package:delivery_app_livreur/views/login/login_controller.dart';
import 'package:delivery_app_livreur/widgets/loading_popup.dart';
import 'package:flutter/material.dart';

import '../../widgets/app_button.dart';
import '../../widgets/app_text_fields.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginController loginController = LoginController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Image.asset(
                            'assets/logo/delivery_app_livreur_logo.png'),
                        RichText(
                          text: const TextSpan(
                            children: [
                              TextSpan(
                                text: 'Welcome to ',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              TextSpan(
                                text: 'delivery platform',
                                style: TextStyle(
                                  color: Color(0xff61C002),
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              'Login now',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            const Text(
                              'Please enter your credentials',
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            SizedBox(
                              child: AppTextField(
                                placeholder: 'Email',
                                controller: loginController.emailTextController,
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            AppTextField(
                              placeholder: 'Password',
                              controller:
                                  loginController.passwordTextController,
                              obscureText: true,
                            ),
                            const SizedBox(
                              height: 40,
                            ),
                            AppButton(
                              buttonText: 'Sign in',
                              onTap: _onSubmitPressed,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onSubmitPressed() {
    bool validationResult = loginController.verifyInput();
    if (!validationResult) {
      const LoadingPopup().show(context);
      loginController.loginUser().then((result) {
        Navigator.pop(context);
        FocusScope.of(context).requestFocus(FocusNode());
        if (result != null) {
          Navigator.pushReplacementNamed(context, '/container');
        } else {
          context.displayErrorSnackbar('Veuillez vérifier vos coordonnées');
        }
      });
    } else {
      context.displayErrorSnackbar('Veuillez entrer tout vos coordonnées');
    }
  }
}
