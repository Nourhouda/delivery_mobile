import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/login_repository.dart';
import 'package:flutter/cupertino.dart';

class LoginController {
  final TextEditingController emailTextController = TextEditingController();
  final TextEditingController passwordTextController = TextEditingController();

  bool verifyInput() {
    return emailTextController.text.isEmpty ||
        passwordTextController.text.isEmpty;
  }

  Future<UserModel?> loginUser() async {
    return LoginRepository().loginUser(
      emailTextController.text,
      passwordTextController.text,
    );
  }
}
