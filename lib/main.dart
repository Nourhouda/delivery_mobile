import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/views/container/container_screen.dart';
import 'package:delivery_app_livreur/views/login/login_screen.dart';
import 'package:delivery_app_livreur/views/orders/order_detail_screen.dart';
import 'package:delivery_app_livreur/views/orders/orders_list_controller.dart';
import 'package:delivery_app_livreur/views/profile/profile_info_controller.dart';
import 'package:delivery_app_livreur/views/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ProfileInfoController(),
        ),
        ChangeNotifierProvider(
          create: (context) => OrdersListController(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'DMSans',
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xff290263),
      ),
      onGenerateRoute: _generateRoutes,
      routes: {
        '/': ((context) => const SplashScreen()),
        '/login': ((context) => const LoginScreen()),
        '/container': ((context) => const ContainerScreen()),
      },
    );
  }

  Route<dynamic> _generateRoutes(settings) {
    if (settings.name == '/order-detail') {
      final args = settings.arguments as OrderModel;
      return MaterialPageRoute(
        builder: (context) {
          return OrderDetailScreen(
            orderModel: args,
          );
        },
      );
    } else {
      return MaterialPageRoute(
        builder: (context) {
          return const Scaffold();
        },
      );
    }
  }
}
