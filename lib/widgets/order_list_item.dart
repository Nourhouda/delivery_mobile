import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:flutter/material.dart';

class OrderListItem extends StatelessWidget {
  final OrderModel orderModel;

  const OrderListItem({
    Key? key,
    required this.orderModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        color: Colors.grey[100]
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 10,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(orderModel.client?.adresse ?? 'Adresse: N/A'),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    orderModel.client?.fullName ?? '',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text('${orderModel.produits.length} Articles'),
                ],
              ),
            ),
            Text('${orderModel.totalAmount()} TND')
          ],
        ),
      ),
    );
  }
}
