// import 'package:flutter/material.dart';

// import '../constants/colors.dart';

// class AppTextField extends StatelessWidget {
//   final String placeholder;
//   final TextEditingController controller;
//   final bool obscureText;
//   final bool enabled;
//   final double height;

//   const AppTextField({
//     Key? key,
//     required this.placeholder,
//     required this.controller,
//     this.obscureText = false,
//     this.enabled = true,
//     this.height = 60,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: 300,
//       height: height,
//       child: TextField(
//         enabled: enabled,
//         controller: controller,
//         obscureText: obscureText,
//         maxLines: 10,
//         style: TextStyle(
//           color: enabled ? Colors.black : Colors.grey,
//           fontSize: 15,
//         ),
//         decoration: InputDecoration(
//           label: Text(placeholder),
//           fillColor: const Color(0xffF3F3F3),
//           filled: true,
//           enabledBorder: OutlineInputBorder(
//             borderSide: enabled
//                 ? const BorderSide(
//                     color: secondaryColor,
//                   )
//                 : const BorderSide(
//                     color: Color(0xffF3F3F3),
//                   ),
//             borderRadius: BorderRadius.circular(10),
//           ),
//           focusedBorder: OutlineInputBorder(
//             borderSide: enabled
//                 ? const BorderSide(
//                     color: secondaryColor,
//                   )
//                 : const BorderSide(
//                     color: Color(0xffF3F3F3),
//                   ),
//             borderRadius: BorderRadius.circular(10),
//           ),
//         ),
//       ),
//     );
//   }
// }
import 'package:flutter/material.dart';

import '../constants/colors.dart';

class AppTextField extends StatelessWidget {
  final String placeholder;
  final TextEditingController controller;
  final bool obscureText;
  final bool enabled;
  final double height;

  const AppTextField({
    Key? key,
    required this.placeholder,
    required this.controller,
    this.obscureText = false,
    this.enabled = true,
    this.height = 60,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      height: height,
      child: TextField(
        enabled: enabled,
        controller: controller,
        obscureText: obscureText,
        style: TextStyle(
          color: enabled ? Colors.black : Colors.grey,
          fontSize: 15,
        ),
        decoration: InputDecoration(
          label: Text(placeholder),
          fillColor: const Color(0xffF3F3F3),
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderSide: enabled
                ? const BorderSide(
                    color: secondaryColor,
                  )
                : const BorderSide(
                    color: Color(0xffF3F3F3),
                  ),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: enabled
                ? const BorderSide(
                    color: secondaryColor,
                  )
                : const BorderSide(
                    color: Color(0xffF3F3F3),
                  ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
