import 'package:flutter/material.dart';

class StatusChooserWidget extends StatefulWidget {
  final Function(String) onConfirmPressed;

  const StatusChooserWidget({
    Key? key,
    required this.onConfirmPressed,
  }) : super(key: key);

  @override
  State<StatusChooserWidget> createState() => _StatusChooserWidgetState();

  void show(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('Changer status'),
          content: this,
        );
      },
    );
  }
}

class _StatusChooserWidgetState extends State<StatusChooserWidget> {
  var status = ['Livré', 'Non livré'];
  var statusValue = ['livre', 'nonlivre'];
  late var selectedStatus = status.first;
  late var selectedStatusValue = statusValue.first;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Text(
          'Choissisez une des status ci-dessous, cette action est irréversible',
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.5,
          decoration: BoxDecoration(
              color: Colors.grey[100], borderRadius: BorderRadius.circular(5)),
          child: DropdownButton<String>(
            isExpanded: true,
            underline: const SizedBox.shrink(),
            value: selectedStatus,
            items: List<DropdownMenuItem<String>>.from(
              status.map(
                (e) => DropdownMenuItem<String>(
                  child: Text(e),
                  value: e,
                ),
              ),
            ),
            onChanged: (vl) {
              setState(() {
                selectedStatus = vl ?? status.first;
                selectedStatusValue = statusValue[status.indexOf(selectedStatus)];
              });
            },
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Annuler'),
            ),
            const SizedBox(
              width: 10,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
                widget.onConfirmPressed.call(selectedStatusValue);
              },
              child: const Text('Confirmer'),
            ),
          ],
        )
      ],
    );
  }
}
