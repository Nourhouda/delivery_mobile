import 'package:delivery_app_livreur/data/models/store_model.dart';
import 'package:delivery_app_livreur/data/models/user_model.dart';

class OrderModel {
  late String id;
  late String idClient;
  late UserModel? client;
  late String idVendeur;
  late String status;
  late List<Produits> produits;
  late String createdDate;
  late StoreModel? store;

  double totalAmount() {
    double somme = 0;
    for (var product in produits) {
      somme += product.produit.prix * product.quantity;
    }
    return somme;
  }

  String shortId() {
    return id.substring(id.length - 4, id.length);
  }

  String getStatusLabel() {
    String result = "";
    if (status == "encourliv") {
      result = "En cours de livraison";
    } else if (status == "livre") {
      result = "Livré";
    } else if (status == "nonlivre") {
      result = "Non Livré";
    } else if (status == "prete") {
      result = "Préte à livré";
    }
    // else if (status == "") {
    //   result = "";
    // }
    return result;
  }

  OrderModel.fromJson(
    Map<String, dynamic> json,
    UserModel? clientModel,
    StoreModel? storeModel,
  ) {
    id = json['id'];
    idClient = json['idClient'];
    idVendeur = json['idVendeur'];
    status = json['status'] ?? '';
    if (json['produits'] != null) {
      produits = <Produits>[];
      json['produits'].forEach((v) {
        produits.add(Produits.fromJson(v));
      });
    }
    createdDate = json['createdDate'];
    client = clientModel;
    store = storeModel;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['idClient'] = idClient;
    data['idVendeur'] = idVendeur;
    data['status'] = status;
    data['produits'] = produits.map((e) => e.toJson()).toList();
    data['createdDate'] = createdDate;
    return data;
  }
}

class Produits {
  late Produit produit;
  late int quantity;

  Produits.fromJson(Map<String, dynamic> json) {
    produit = Produit.fromJson(json['produit']);
    quantity = json['quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['quantity'] = quantity;
    data['produit'] = produit.toJson();
    return data;
  }
}

class Produit {
  late String id;
  late String nomProd;
  late double prix;

  Produit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nomProd = json['nom_prod'];
    prix = json['prix'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['nom_prod'] = nomProd;
    data['prix'] = prix;
    return data;
  }
}
