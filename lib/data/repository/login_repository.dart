import 'dart:convert';

import 'package:delivery_app_livreur/constants/shared_prefs_keys.dart';
import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/app_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginRepository extends AppRepository {
  Future<UserModel?> loginUser(String email, String password) async {
    try {
      var serverResponse = await dio.post('/api/v1/login-livreur',
          data: {'email': email, 'password': password});
      var user = UserModel.fromJson(serverResponse.data['data']);
      var sharedPrefs = await SharedPreferences.getInstance();
      sharedPrefs.setString(userKey, jsonEncode(user));
      return user;
    } catch (e) {
      return null;
    }
  }

  Future<UserModel?> updateUser(
    String name,
    String lastname,
    String adresse,
  ) async {
    try {
      var sharedPrefs = await SharedPreferences.getInstance();
      var userInShared = sharedPrefs.getString(userKey);
      var userModel = UserModel.fromJson(jsonDecode(userInShared ?? ''));
      userModel.lastName = lastname;
      userModel.firstname = name;
      userModel.adresse = adresse;
      await dio.put(
        '/api/v1/livreur/${userModel.id}',
        data: userModel.toJson(),
      );
      sharedPrefs.setString(userKey, jsonEncode(userModel));
      return userModel;
    } catch (e) {
      return null;
    }
  }

  Future<dynamic> updateUserStatus() async {
    try {
      var sharedPrefs = await SharedPreferences.getInstance();
      var userInShared = sharedPrefs.getString(userKey);
      var userModel = UserModel.fromJson(jsonDecode(userInShared ?? ''));
      var serverResponse =
          await dio.put('/api/v1/livreur/status/${userModel.id}');
      return serverResponse.data;
    } catch (e) {
      return null;
    }
  }
}
