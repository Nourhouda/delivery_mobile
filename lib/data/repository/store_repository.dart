import 'package:delivery_app_livreur/data/models/store_model.dart';
import 'package:delivery_app_livreur/data/repository/app_repository.dart';

class StoreRepository extends AppRepository {
  
  Future<StoreModel?> getStoreBySeller(String sellerId) async {
    try {
      var serverResponse = await dio.get('/api/v1/vendeur/$sellerId');
      var seller = serverResponse.data['data'];
      var storeRequestResponse =
          await dio.get('/api/v1/boutique/${seller['idBoutique']}');
      return StoreModel.fromJson(storeRequestResponse.data['data']);
    } catch (e) {
      return null;
    }
  }

}