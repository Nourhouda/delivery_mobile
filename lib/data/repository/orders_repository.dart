import 'dart:convert';

import 'package:delivery_app_livreur/constants/shared_prefs_keys.dart';
import 'package:delivery_app_livreur/data/models/order_model.dart';
import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/app_repository.dart';
import 'package:delivery_app_livreur/data/repository/store_repository.dart';
import 'package:delivery_app_livreur/data/repository/users_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrdersRepository extends AppRepository {
  Future<List<OrderModel>> getListOrders() async {
    try {
      var sharedPrefs = await SharedPreferences.getInstance();
      var userInShared = sharedPrefs.getString(userKey);
      var userModel = UserModel.fromJson(jsonDecode(userInShared ?? ''));
      var serverResponse =
          await dio.get('/api/v1/commande/livreur/${userModel.id}');
      var data = serverResponse.data['data'];
      List<OrderModel> orders = [];
      for (var item in data) {
        var userModel = await UsersRepository().getClientById(item['idClient']);
        var storeModel =
            await StoreRepository().getStoreBySeller(item['idVendeur']);
        orders.add(OrderModel.fromJson(item, userModel, storeModel));
      }
      return orders;
    } catch (e) {
      return [];
    }
  }

  Future<dynamic> updateOrderStatus(OrderModel order) async {
    try {
      var serverResponse = await dio.put(
        '/api/v1/commande/${order.id}',
        data: order.toJson(),
      );
      return serverResponse.data;
    } catch (e) {
      return null;
    }
  }
}
