import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/app_repository.dart';

class UsersRepository extends AppRepository {
  
  Future<UserModel?> getClientById(String id) async{
    try {
      var serverResponse = await dio.get('/api/v1/client/$id');
      return UserModel.fromJson(serverResponse.data['data']);
    } catch (e) {
      return null;
    }
  }

}