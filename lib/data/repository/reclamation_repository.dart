import 'dart:convert';

import 'package:delivery_app_livreur/constants/shared_prefs_keys.dart';
import 'package:delivery_app_livreur/data/models/reclamation_model.dart';
import 'package:delivery_app_livreur/data/models/user_model.dart';
import 'package:delivery_app_livreur/data/repository/app_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReclamationRepository extends AppRepository {
  
  Future<ReclamationModel?> createNewReclamation(
      String titre, String description) async {
    try {
      var sharedPrefs = (await SharedPreferences.getInstance());
      var user = UserModel.fromJson(
        jsonDecode((sharedPrefs.getString(userKey)) ?? ''),
      );
      var serverReponse = await dio.post('/api/v1/add-reclamation', data: {
        "titre": titre,
        "description": description,
        "type": "systeme",
        "idClient": user.id,
        "status": "new"
      });
      return ReclamationModel.fromJson(serverReponse.data['data']);
    } catch (e) {
      return null;
    }
  }

}