import 'package:flutter/material.dart';

extension ContextExtensions on BuildContext {
  displaySnackbar(String message, Color color) {
    ScaffoldMessenger.of(this).showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: color,
      ),
    );
  }

  displayErrorSnackbar(String message) {
    displaySnackbar(message, Colors.red);
  }
}
